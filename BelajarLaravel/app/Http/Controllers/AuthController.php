<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome()
    {
        return view('welcome');
    }

    public function kirim(Request $request)
    {
        $nameD = $request['nameD'];
        $nameB = $request['nameB'];
        $jenisKelamin = $request['jk'];
        $alamat = $request['alamat'];

        return view('welcome',['nameD'=>$nameD, 'nameB'=>$nameB, 'jenisKelamin'=>$jenisKelamin, 'alamat'=> $alamat]);
    }
}
